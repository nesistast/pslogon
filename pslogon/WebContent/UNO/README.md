#H1 NeSIS PeopleSoft Logon Page Setup
The NeSIS logon page is setup so that campuses may manage all their code on Campus Content.
Portal Administrators should access their campus folder and look for a folder called pslogon.
In the pslogon folder their are four folders one for each PeopleSoft environment.  This ensures
changes to the logon page can be properly tested when changes are made.  The index.html page is 
the content that will display when a user accesses PeopleSoft under their campus site.

#H2 Minimal Setup
The simplest way to set up the logon page is to do nothing.  The page is ready to go as is.

#H2 Moderate Setup
Portal Administrators can change much of the content such as labels and related links by modifying 
the config.js file in the js folder.  Color and style changes can be made in the theme.css file 
located in the css folder. The base CSS as delivered comes from [Twitter Bootstrap](http://getbootstrap.com/) 
and their website is a good resource for styling assistance.  Available icons for the related links are 
found on the components page.  

#H3 Advanced Setup
Build whatever you want.  The basic folder structure must remain as is, and the config.js file is
needed if you want to control styles and meta data on the parent frame which is deployed on the
PeopleSoft web server.  Past that any index.html page will work provided the form field names are the same
and the url submitted to matches the correct PeopleSoft environment.  Two parameters host and site are passed 
to index.html that can be used to create a dynamic action attribute for the form.    