nba.funcs.configure({	
	description:'The Nebraska Student Information System services student, faculty and staff users for both the university and state college systems.',
	author:'Technical Appliction Support Team',
	copyright:'2014 University of Nebraska Board of Regents',
	keywords:'nebraska, student information system, nesis, enrollment, financial aid, admissions, student accounts, campus community, enrollment, academics',
	favicon:'favicon.ico',
	iframeCSS:'theme.css',
	title:'Campus Solutions Login',
	
	userLabel:'NUID',
	passwordLabel:'Password',
	submitButton:'Log In',
	forgotPasswordLink:{
		href:'https://trueyou.nebraska.edu/idm/user/login.jsp',
		html:'Forgot Password',
		target:'_blank'
	},
	related:[
         {
        	 href:'c/COMMUNITY_ACCESS.CLASS_SEARCH.GBL',
        	 html:'Open Class Search',
        	 target:'_blank',
        	 icoClass:'glyphicon glyphicon-search'
         },
         {
        	 href:'https://ntouch.nebraska.edu',
        	 html:'Open nTouch',
        	 target:'_blank',
        	 icoClass:'glyphicon glyphicon-folder-open'
         },
         {
        	 href:'https://campuscontent.nebraska.edu/nesis/calendar.html',
        	 html:'Open Amp Calendar',
        	 target:'_blank',
        	 icoClass:'glyphicon glyphicon-calendar'
         },
         {
        	 href:'https://heuc.nebraska.edu',
        	 html:'Open Heuc Website',
        	 target:'_blank',
        	 icoClass:'glyphicon glyphicon-globe'
         }   
	],
	securityStatement:'For security reasons please log out and exit your web browser when you are finished accessing services that require authentication!'
});