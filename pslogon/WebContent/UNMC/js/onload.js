if(!window.nba){window.nba = {funcs:{}};};
window.nba.funcs.configure = function(cfg){	
	//Make common domain
	document.domain = "nebraska.edu";
	
	//parameterize querystring
	var queryString = {};
	window.location.href.replace(
		new RegExp("([^?=&]+)(=([^&]*))?", "g"),
	    function($0, $1, $2, $3) { queryString[$1] = $3;}	
	);
	
	//generate environment url;
	var url = 'https://' + queryString['host'] + '/psp/' + queryString['host'].split('.')[0] + '/' + queryString['site'] + '/HRMS/';
	
	//getcookie by name
	function getCookie(name) {
		var value = "; " + document.cookie;
		var parts = value.split("; " + name + "=");
		if (parts.length == 2) 
			return parts.pop().split(";").shift();
	}
	
	//function creates dynamic related links
	var createRelatedLinks = function(opts){
		var i,l,html='';
		if(opts instanceof Array){			
			for(i=0,l=opts.length; i<l; i++){
				var href = (opts[i].href.indexOf('http') === 0) ? opts[i].href : url + opts[i].href; 
				html += '<li><a href="' + href + '" title="' + opts[i].html + '" target="' + opts[i].target + '">';
				html += '<span class="sr-only">' + opts[i].html + '</span>';
				html += '<span class="' + opts[i].icoClass + '"></span>';
				html += '</a></li>';				
			}
			return html;
		}
	};
	
	//replace page elements
	var load = function(){
		var arr,i,l,f;
		var forms = document.getElementsByTagName('form');
		
		
		if(!queryString['host']){
			console.log('missing host');
			return;
		}
		
		if(!queryString['site']){
			console.log('missing site');
			return;
		}
		
		//Must get to here for working login
		for(i=0,l=forms.length; i<l; i++){
			if(forms[i].id === ''){
				var user = getCookie('SignOnDefault');
				if(user){
					forms[i].userid.value = user;					
				}
				forms[0].userid.focus();							
			}
			forms[i].action = 'https://' + queryString['host'] + '/psp/' + queryString['host'].split('.')[0] + '/' + queryString['site'] + '/HRMS/cmd=login&languageCd=ENG';
		}
		
		if(!cfg){
			console.log('namespace missing');
			return;
		}
		
		
		//For campuses wishing to load from configuration
		if(cfg){
			if(document.querySelectorAll){			
				for(setting in cfg){
					arr = document.querySelectorAll('*[data-config="' + setting + '"]');
					for(i=0,l=arr.length; i<l; i++){
						switch(arr[i].nodeName){
						case 'META':
							arr[i].content = cfg[setting];
							break;
						case 'LINK':
							f = (arr[i].rel === 'stylesheet') ? 'css/' : 'img/';
							arr[i].href = f + cfg[setting];
							break;
						case 'INPUT':
							arr[i].value = cfg[setting];
							break;
						case 'A':
							arr[i].href = cfg[setting].href;
							arr[i].target = cfg[setting].target;
							arr[i].innerHTML = cfg[setting].html;
							break;
						case 'UL':
							if(setting === 'related')
								arr[i].innerHTML = createRelatedLinks(cfg[setting]);								
							break;
						default:
							arr[i].innerHTML = cfg[setting];
						}
					}
				}
			}else{
				console.log('unsupported browser');
			}		
		}else{
			console.log('configuration missing');
		}
	};
	
	//Let it fail all the way back.
	if(document.addEventListener){
		window.addEventListener('load',load,false);
	}else if(document.attachEvent){
		window.attachEvent('onload',load);
	}else{
		window.onload = load;
	}
};